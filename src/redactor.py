#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# PDF Redaction
#
# (C) 2022 Andres Solis Montero
# -----------------------------------------------------------
import fitz 
import argparse
from os.path import basename, dirname, join
from modules.redactors import RectRedactor, RegExRedactor, SectionRedactor


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("inputs", nargs='+', help="input files")
    parser.add_argument('--output-suffix', default="redacted", type=str)
    parser.add_argument('--annotations'  , nargs='?', const="./config/annotations.json", type=str)
    parser.add_argument('--expressions'  , nargs='?', const="./config/expressions.json", type=str)
    parser.add_argument('--elements'     , nargs='?', const="./config/elements.json"   , type=str)
    args = parser.parse_args()

    redactors = {
        args.annotations: RectRedactor(args.annotations),
        args.expressions: RegExRedactor(args.expressions),
        args.elements   : SectionRedactor(args.elements)
    }

    for _input_ in args.inputs:
        doc = fitz.open(_input_)
        if args.expressions:
            doc = redactors[args.expressions].redact(doc)
        if args.annotations:
            doc = redactors[args.annotations].redact(doc)
        if args.elements:
            doc = redactors[args.elements].redact(doc)

        filename = ".".join(basename(_input_).split('.')[0:-1])
        output_file = f"{filename}.{args.output_suffix}.pdf"
        doc.save(join(dirname(_input_), output_file))
        print(f"Successfully redacted: {output_file}")















