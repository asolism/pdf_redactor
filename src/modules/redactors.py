# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# PDF Redaction
#
# (C) 2022 Andres Solis Montero
# -----------------------------------------------------------

import fitz
import re
from tqdm import tqdm
from config.models import *
from os.path import exists

TEXT = '**********'
FILL_COLOR = (0,0,0)
TEXT_COLOR = (0,0,0)
MAX_HEIGHT_PTS = 792
MAX_WIDTH_PTS = 612


class Redactor(object):
    def redact(self, doc :fitz.fitz.Document) -> fitz.fitz.Document:
        pass

class RegExRedactor(Redactor):
    # static methods work independent of class object
    @staticmethod
    def find_regex_expr(lines, expressions: Expressions):

        """ Function to get all the lines """

        for line in lines:
            for match in expressions.matches:
                search = re.search(match.regex, line, re.IGNORECASE)
                if search:
                    yield search.group(match.group)

    def __init__(self, expressions_file: str ) -> None:
        super().__init__()
        if expressions_file and exists(expressions_file):
            self.expressions = Expressions.parse_file(expressions_file)

    def redact(self, doc: fitz.fitz.Document) -> fitz.fitz.Document:
        for page in tqdm(doc):

            # _wrapContents is needed for fixing
            # alignment issues with rect boxes in some
            # cases where there is alignment issue
            page.wrap_contents()

            # getting the rect boxes which consists the matching regexs
            sensitive = self.find_regex_expr(page.get_text("text")
                                                 .split('\n'), self.expressions)
            for data in sensitive:
                areas = page.search_for(data)

                # drawing outline over sensitive datas
                [page.add_redact_annot(area,
                                        text=TEXT,
                                        text_color=TEXT_COLOR, 
                                        fill = FILL_COLOR) for area in areas]
            # applying the redaction
            page.apply_redactions()
        return doc

class RectRedactor(Redactor):
    def __init__(self, annotations_file:str) -> None:
        super().__init__()
        if annotations_file and exists(annotations_file):
            self.annotations = Annotations.parse_file(annotations_file)

    def redact(self, doc: fitz.fitz.Document) -> fitz.fitz.Document:
        for page_no, page in tqdm(enumerate(doc)):
                # wrap_contents is needed for fixing
                # alignment issues with rect boxes in some
                # cases where there is alignment issue
                page.wrap_contents()
                if page_no in self.annotations.pages:
                    for r in self.annotations.pages[page_no]:
                        page.add_redact_annot(fitz.Rect(r.x0,r.y0,r.x1,r.y1), 
                                       fill=FILL_COLOR,
                                       text=TEXT,
                                       text_color=TEXT_COLOR)

                page.apply_redactions()
        return doc

class SectionRedactor(Redactor):
    def __init__(self, section_file:str) -> None:
        super().__init__()
        if section_file and exists(section_file):
            self.sections = Elements.parse_file(section_file).sections

    def redact(self, doc: fitz.fitz.Document) -> fitz.fitz.Document:
        for e in tqdm(self.sections):
            targets, nexts = [], []
            for index, page in tqdm(enumerate(doc)):
                page.wrap_contents()
                targets.extend([(index, e.x1, e.width, e.section, area)
                                         for area in page.search_for(e.target)])
                nexts.extend([(index, area) 
                                         for area in page.search_for(e.next_)])

            for index, page in enumerate(doc):
                page.wrap_contents()
                for (p1, x1, w1, s1, rect1), (p2, rect2) in zip(targets, nexts):
                    new_rect = None
                    if (p1==p2==index):
                        new_rect = fitz.Rect(x1, rect1[3] if s1 else rect1[1], x1+w1, rect2[1])
                    elif (p1==index) and (p2==p1+1):
                        new_rect = fitz.Rect(x1, rect1[3] if s1 else rect1[1], x1+w1, MAX_HEIGHT_PTS)
                    elif (p2==index) and (p1==p2-1):
                        new_rect = fitz.Rect(x1, 0, x1+w1, rect2[1])
                    if new_rect is not None:
                        # drawing outline over sensitive datas
                        page.add_redact_annot(new_rect, 
                                              text='******',
                                              text_color=TEXT_COLOR,
                                              fill = TEXT_COLOR)
                page.apply_redactions()
        return doc

