# -*- coding: utf-8 -*-
# -----------------------------------------------------------
# PDF Redaction
#
# (C) 2022 Andres Solis Montero
# -----------------------------------------------------------
from pydantic import BaseModel, Field
from typing import Dict, List, Optional

class Rect(BaseModel):
    x0: int = 0
    y0: int = 0
    x1: int = 0
    y1: int = 0

class Annotations(BaseModel):
    pages: Dict[int, List[Rect]] = Field(default_factory=dict)

class Match(BaseModel):
    group: int  = 0
    regex: Optional[str]
    
class Expressions(BaseModel):
    matches: List[Match] = Field(default_factory=list())
        
class Section(BaseModel):
    target: str 
    next_: str = Field(alias="next")
    x1: int = 0
    width: int = 0
    section: bool = True
    
    class Config:
        allow_population_by_field_name = True
        
class Elements(BaseModel):
    sections: List[Section] = Field(default_factory=list)

    