## PDF Redactor

PDF tool to redact pdf text. Allows to specify multiple regular expressions or rectangular page regions to remove 
information from while creating a blackout rectangle in the extracted region. 

### Quick Usage Guide 

Recreate the conda environment.yml 
```bash
$cd src
$conda env create -f environment.yml
$conda activate pdf-redactor
```bash  


```bash
# It will read all the annotations inside the config/annotations.json file and will apply to the input pdf files
src$ ./redactor.py [<input_pdf>|...]  --annotations 

# It will search for all the regular expressions inside the config/expressions.json file and will remove them from 
# the pdf file
src$ ./redactor.py [<input_pdf>|...]  --expressions

# It will search for the regular expresions and annotations areas, it will apply both
src$ ./redactor.py [<input_pdf>|...]  --expressions --annotations
```

Run example: 
```bash
src$ ./redactor.py example/example1.pdf example/example2.pdf --annotations
```


### File: annotations.json

The annotations files contains a dictionary of pages. For each 0-based index page , it will contain a list of rectangular regions to be 
blackout. 

```python
annotations = {
    0: [
        {x0: <x0_coord>, y0: <yo_coord>, x1: <x1_coord>, y1: <y1_coord>},
        ...
    ],
    1: [
        {x0: <x0_coord>, y0: <yo_coord>, x1: <x1_coord>, y1: <y1_coord>},
        ...
    ]
}

#### File: expressions.json
The expression files contain a list of matches. A match is composed of a regex and a group number. It allow to capture regex group matches by index. 

For example, for a regular expression trying to match an email `"(.\*)@(\.*)"` if group 0 is specified, the whole match will be blackout, but if group has a value of 1, it will only blackout the username, if a value of 2 is specified then the domain part will be blackout. 

```python
{
    matches:[
        {
            group: 0,
            regex: "(\.*)@(\.*)"
        }, 
        ...
    ] 
}
```

### Output Example

![output](doc/output.example.png)

### Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

### License

Unless otherwise specified, the source code of this project is covered under Crown Copyright, Government of Canada, and is distributed under the [MIT License](LICENSE).


______________________

#TODO: translate instructions to French

### Comment contribuer

Voir [CONTRIBUTING.md](CONTRIBUTING.md)

### License

Sauf indication contraire, le code source de ce projet est protégé par le droit de la Couronne du gouvernement du Canada et distribué sous la [LICENSE](license MIT).

