([Français](#sécurité))

# Security

**Do not post any security issues on the public repository!** Security vulnerabilities must be reported by email to `andres@solism.ca`

____________________

## Sécurité

**Ne publiez aucun problème de sécurité sur le dépôt publique!** Les vulérabilités de sécurté doivent être signalées par courriel à `andres@solism.ca`